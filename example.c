#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/random.h>
#include <linux/kthread.h>
#include <linux/bitops.h>
#include <linux/completion.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"

static int list_size = 1;

module_param(list_size, int, 0);
MODULE_PARM_DESC(list_size, "Base size of list");

typedef struct List {
    struct List* next;
    struct List* prev;
} List;

typedef struct ListNode {
    List list;
    unsigned int value;
} ListNode;

typedef enum ListDirection {
    Forward,
    Reverse
} ListDirection;

typedef struct FunctionParams {
    List* head;
    ListDirection direction;
    volatile bool running;
    struct completion completion;
} FunctionParams;

static bool add_node(List* head, unsigned int value) {
    ListNode* node = (ListNode*)kmalloc(sizeof(ListNode), GFP_KERNEL);

    if (node == NULL) {
        return false;
    }

    node->value = value;
    node->list.next = head;


    List* last_node = head->prev;
    last_node->next = &node->list;
    node->list.prev = last_node;
    head->prev = &node->list;

    return true;
}

static inline
bool list_is_empty(List* head) {
    return head->next == head;
}

static List* pop(List* head, ListDirection direction) {
    if (list_is_empty(head)) {
        return NULL;
    }

    switch (direction) {
        case Forward: {
            List* node = head->next;
            node->next->prev = head;
            head->next = node->next;
            return node;
        }
        case Reverse: {
            List* node = head->prev;
            node->prev->next = head;
            head->prev = node->prev;
            return node;
        }
        default:
            return NULL;
    }
}

static void destroy_list(List* head) {
    if (head != NULL) {
        List* node = pop(head, Forward);
        while(node != NULL) {
            kfree(node);
            node = pop(head, Forward);
        }
        kfree(head);
    }
}

static List* generate_list(int size) {
    List* head = (List*)kmalloc(sizeof(List), GFP_KERNEL);

    if (head == NULL) {
        return head;
    }

    head->next = head;
    head->prev = head;

    int index;
    for(index = 0; index < size; ++index) {
        unsigned int value;
        get_random_bytes(&value, sizeof(value));
        if (!add_node(head, value)) {
            destroy_list(head);
            return NULL;
        }
    }

    return head;
}

static inline int count_1bits(unsigned int value) {
    return hweight32(value);
}

static inline int count_0bits(unsigned int value) {
    return BITS_PER_BYTE * sizeof(value) - hweight32(value);
}

static struct mutex process_mutex;

static int process_list(void* params) {
    if (params == NULL) {
        return 0;
    }

    FunctionParams* function_params = (FunctionParams*)params;
    List* head = function_params->head;

    int node_counter = 0;
    int bits = 0;

    printk(KERN_INFO "Entering into %s loop\n", function_params->direction == Forward ? "Forward" : "Reverse");
    switch (function_params->direction) {
        case Forward: {
            while(function_params->running) {
                mutex_lock(&process_mutex);
                List* current_node = pop(head, function_params->direction);
                mutex_unlock(&process_mutex);
                if (current_node == NULL) {
                    break;
                }
                ++node_counter;
                bits += count_0bits(((ListNode*)current_node)->value);
                kfree(current_node);
            }
            break;
            
        }
        case Reverse: {
            while(function_params->running) {
                mutex_lock(&process_mutex);
                List* current_node = pop(head, function_params->direction);
                mutex_unlock(&process_mutex);
                if (current_node == NULL) {
                    break;
                }
                ++node_counter;
                bits += count_1bits(((ListNode*)current_node)->value);
                kfree(current_node);
            }
            break;
        }
        default: {
            function_params->running = false;
            complete(&function_params->completion);
            return 1;
        }
    }
    function_params->running = false;
    printk(KERN_INFO "Handled nodes: %d, %s bits: %d\n", node_counter, function_params->direction == Forward ? "zero" : "one", bits);
    complete(&function_params->completion);
    return 0;
}

static List* list = NULL;

static FunctionParams next_direction;
static FunctionParams prev_direction;

static struct task_struct* thread1 = NULL;
static struct task_struct* thread2 = NULL;

static int __init module_enter(void) {
    if (list_size < 1) {
        printk(KERN_ERR "Param list_size can't be less than 1\n");
        return 1;
    }

    mutex_init(&process_mutex);
    list = generate_list(list_size);
    if (list == NULL) {
        printk(KERN_ERR "Can't create list\n");
        return 1;
    }

    next_direction.head = list;
    next_direction.direction = Forward;
    next_direction.running = true;
    init_completion(&next_direction.completion);

    prev_direction.head = list;
    prev_direction.direction = Reverse;
    prev_direction.running = true;
    init_completion(&prev_direction.completion);

    thread1 = kthread_create(process_list, &next_direction, "Next iteration");
    thread2 = kthread_create(process_list, &prev_direction, "Previous iteration");

    if (thread1) {
        wake_up_process(thread1);
    } else {
        printk(KERN_ERR "Cannot create kthread 1\n"); 
    }

    if (thread2) {
        wake_up_process(thread2);
    } else {
        printk(KERN_ERR "Cannot create kthread 2\n"); 
    }

    if (thread1 == NULL && thread2 == NULL) {
        printk(KERN_ERR "Cannot create both threads\n"); 
        return 1;
    }

	return 0;
}

static void __exit module_cleanup(void) {
    if (thread1 != NULL && next_direction.running) {
        next_direction.running = false;
        wait_for_completion(&next_direction.completion);
    }

    if (thread2 != NULL && prev_direction.running) {
        prev_direction.running = false;
        wait_for_completion(&prev_direction.completion);
    }

    destroy_list(list);
}

module_init(module_enter);
module_exit(module_cleanup);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jan Saliy");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

#pragma GCC diagnostic pop
